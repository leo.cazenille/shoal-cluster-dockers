#!/bin/bash
set -e

uid=$1 #${1:-1000}
shift;

useradd -d /home/user -Ms /bin/bash -u $uid user
chown -R $uid /home/user

# Launch program
exec gosu user bash -c "cd /home/user/; ./pi.R $@"

# MODELINE	"{{{1
# vim:expandtab:softtabstop=4:shiftwidth=4:fileencoding=utf-8
# vim:foldmethod=marker
