#!/bin/bash

# ./runDockerExpe.sh config priority image
# Parameters:
#  first parameter: name of the configuration file (default "conf/configuration.yaml")
#  second parameter: priority (low, mid, high, veryhigh). Please DO NOT use "veryhigh" without authorisation.
#  third parameter: name of the docker image (default "shoal-r-pi:latest")

configFile=${1:-conf/configuration.yaml}
priorityLevel=${2:-"low"}
memoryLimit=8G
resultsPath=$(pwd)/results
resultsPathInContainer=/home/user/results
imageName=${3:-"shoal-r-pi:latest"}
uid=$(id -u)
confPath=$(pwd)/conf
confPathInContainer=/home/user/conf

if [ ! -d $resultsPath ]; then
    mkdir -p $resultsPath
fi

inDockerGroup=`id -Gn | grep docker`
if [ -z "$inDockerGroup" ]; then
    sudoCMD="sudo"
else
    sudoCMD=""
fi
dockerCMD="$sudoCMD docker"

if [ -d "$confPath" ]; then
    confVolParam="-v $confPath:$confPathInContainer"
else
    confVolParam=""
fi

if [ "$priorityLevel" = "low" ]; then
	priorityParam="-c 128"
elif [ "$priorityLevel" = "mid" ]; then
	priorityParam="-c 512"
elif [ "$priorityLevel" = "high" ]; then
	priorityParam="-c 2048"
elif [ "$priorityLevel" = "veryhigh" ]; then
	priorityParam="-c 131072"
else
	priorityParam="-c 128"
fi

exec $dockerCMD run -i -m $memoryLimit --rm $priorityParam -v $resultsPath:$resultsPathInContainer $confVolParam $imageName  "$uid" "-c $configFile"

# MODELINE	"{{{1
# vim:expandtab:softtabstop=4:shiftwidth=4:fileencoding=utf-8
# vim:foldmethod=marker
