#!/bin/bash
set -e

uid=$1 #${1:-1000}
#executionMode=$2 #${2:-"normal"}
shift; #shift;

baseDir="/home/user/shoal-cluster-dockers/python-tf-mnist"
baseCmd="./mnist_with_summaries.py $@ | tee /home/user/results/log.log"

useradd -d /home/user -Ms /bin/bash -u $uid user
chown -R $uid /home/user

# Launch program
exec gosu user bash -c "cd $baseDir; $baseCmd"


# MODELINE	"{{{1
# vim:expandtab:softtabstop=4:shiftwidth=4:fileencoding=utf-8
# vim:foldmethod=marker
