#!/bin/bash

# ./runDockerExpe.sh config priority executionMode image hostnames
# Parameters:
#  first parameter: name of the configuration file (default "conf/configuration.yaml")
#  second parameter: priority (low, mid, high, veryhigh). Please DO NOT use "veryhigh" without authorisation.
#  third parameter: execution mode (normal or cluster).
#  fourth parameter: name of the docker image (default "shoal-python-pi:latest")
#  fifth parameter: list of hosts to launch the images, if execution mode is set to 'cluster'. The list must be split by ",". E.g.: "shoal1,shoal2,shoal3,shoal4,shoal5,shoal6,shoal7,shoal8"

#configFile=${1:-conf/configuration.yaml}
priorityLevel=${1:-"low"}
#executionMode=${3:-"normal"}
imageName=${2:-"shoal-python-tf-mnist:latest"}
#hostsNames=${4:-"shoal1,shoal2,shoal3,shoal4,shoal5,shoal6,shoal7,shoal8"}

#GPUSParams="--gpus all" # TO ALLOW GPUs
GPUSParams="" # TO FORBID GPUs

expeName="shoal-python-tf-mnist" # TO ADAPT
networkName="net-$expeName"
memoryLimit=8G
resultsPath=$(pwd)/results
resultsPathInContainer=/home/user/results
uid=$(id -u)
#confPath=$(pwd)/conf
#confPathInContainer=/home/user/conf

if [ ! -d $resultsPath ]; then
    mkdir -p $resultsPath
fi

inDockerGroup=`id -Gn | grep docker`
if [ -z "$inDockerGroup" ]; then
    sudoCMD="sudo"
else
    sudoCMD=""
fi
dockerCMD="$sudoCMD docker"

#if [ -d "$confPath" ]; then
#    confVolParam="-v $confPath:$confPathInContainer"
#else
#    confVolParam=""
#fi

if [ "$priorityLevel" = "low" ]; then
	priorityParam="-c 128"
elif [ "$priorityLevel" = "mid" ]; then
	priorityParam="-c 512"
elif [ "$priorityLevel" = "high" ]; then
	priorityParam="-c 2048"
elif [ "$priorityLevel" = "veryhigh" ]; then
	priorityParam="-c 131072"
else
	priorityParam="-c 128"
fi


exec $dockerCMD run -i -m $memoryLimit --rm $GPUSParams $priorityParam -v $resultsPath:$resultsPathInContainer $confVolParam $imageName  "$uid"


# MODELINE	"{{{1
# vim:expandtab:softtabstop=4:shiftwidth=4:fileencoding=utf-8
# vim:foldmethod=marker
